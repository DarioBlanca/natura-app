import React, { Component } from 'react'
import { Link } from 'react-router-dom';
const options = ['home', 'recetas', 'cuenta'];

export class Toolbar extends Component {

    state = {
        selected: '',
        options: {
            home: this.props.location == '/inicio' ? '/media/home-verde.svg' : '/media/home-gris.svg',
            recetas: this.props.location == '/inicio/recetas' ? '/media/recetas-verde.svg' : '/media/recetas-gris.svg',
            cuenta: this.props.location == '/inicio/perfil' ? '/media/cuenta-verde.svg' : '/media/cuenta-gris.svg' 
        }
    }

    componentDidMount() {
        let options = {
            home: this.props.location == '/inicio' ? '/media/home-verde.svg' : '/media/home-gris.svg',
            recetas: this.props.location == '/inicio/recetas' ? '/media/recetas-verde.svg' : '/media/recetas-gris.svg',
            cuenta: this.props.location == '/inicio/perfil' ? '/media/cuenta-verde.svg' : '/media/cuenta-gris.svg' 
        }
        this.setState({ options })
    }

    update = (selected) => {
        let options = {
            home: selected == 'home' ? '/media/home-verde.svg' : '/media/home-gris.svg',
            recetas: selected == 'recetas' ? '/media/recetas-verde.svg' : '/media/recetas-gris.svg',
            cuenta: selected == 'cuenta' ? '/media/cuenta-verde.svg' : '/media/cuenta-gris.svg' 
        }
        this.setState({ options, selected });
    }

    selected = (selected) => selected !== this.state.selected && this.update(selected);

    render() {
        console.log(this.props.location);
        return(
            <div className="w-100 shadow-lg bg-white fondo" style={{ position: 'fixed', bottom: '0px', zIndex:3 }}>
                <div className="d-flex bd-highlight" style={{ maxHeight: '30vw' }}>
                    <div className="toolbar-element flex-fill bd-highlight" onClick={() => this.selected('home')}>
                        <Link to="/inicio">
                            <div className="item-container d-flex flex-column justify-content-center" >
                                <img className="toolbar-images" src={this.state.options.home}/>
                                <span className={"toolbar-text unselectable " + (this.state.selected.includes('home') || this.props.location == '/inicio' ? 'text-link-green' : 'text-link-gray')}>
                                    Inicio
                                </span>
                            </div>
                        </Link>
                    </div>
                    <div className="toolbar-element flex-fill bd-highlight" onClick={() => this.selected('recetas')}>
                        <Link to="/inicio/recetas">
                            <div className="item-container d-flex flex-column justify-content-center" >
                                <img className="toolbar-images" src={this.state.options.recetas}/>
                                <span className={"toolbar-text unselectable " + (this.state.selected.includes('recetas') || this.props.location.includes('recetas') ? 'text-link-green' : 'text-link-gray')}>Recetas</span>
                            </div>
                        </Link>
                    </div>
                    <div className="toolbar-element flex-fill bd-highlight" onClick={() => this.selected('cuenta')}>
                        <Link to="/inicio/perfil">
                            <div className="item-container d-flex flex-column justify-content-center">
                                <img className="toolbar-images" src={this.state.options.cuenta}/>
                                <span className={"toolbar-text unselectable " + (this.state.selected.includes('cuenta') || this.props.location.includes('perfil') ? 'text-link-green' : 'text-link-gray')}>Mi cuenta</span>
                            </div>
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}