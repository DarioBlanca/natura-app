import React from 'react'

export const SingleSlide = ({data}) => {
    
    return(
        <div style={{ background: 'white' }}>
            <img className="single-slide-img" src={data.image}/>
            {/* <div className="d-flex justify-content-center mt-3">                
                <h3 className="single-slide-title">{data.title}</h3>
            </div>
            <div className="d-flex justify-content-center mt-1">
                <p className="text-center single-slide-description" style={{...description_styles}}>{data.description}</p> 
            </div> */}
        </div>
    )
}


export default SingleSlide;