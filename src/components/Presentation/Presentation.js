import React, { Component } from 'react';
import data from './data.json';
import { SingleSlide } from './SingleSlide';
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.css";
import { Link, Redirect } from 'react-router-dom';

export class Presentation extends Component {

    state = {
        items: data.sliders.map((element, index) => <SingleSlide data={element} key={index} />),
        selectedItem: 0,
        redirect: false
    }

    next_ref = React.createRef();

    change = (selectedItem) => {
        selectedItem == 2 ? (this.next_ref.current.innerHTML = 'COMENZAR') 
                          : (this.next_ref.current.innerHTML = 'SIGUIENTE');
        this.setState({ ...this.state, selectedItem, redirect: this.state.selectedItem > 2 });
    }

    next = () => this.change(this.state.selectedItem + 1);
    
    render() {
        const config = {
            showThumbs: false,
            showArrows: false,
            showStatus: false,
            showIndicators: true,
            infiniteLoop: false,
            selectedItem: this.state.selectedItem,
            swipeable: true,
            onChange: this.change
        }
        if (this.state.redirect) return (<Redirect to='/inicio'/>); else
        return(
            <div className="w-100">
                <Carousel {...config} className="presentation-mode">
                    {this.state.items}
                </Carousel>
                <div className="d-flex flex-row justify-content-center" style={{ marginTop: '18px' }}>
                    <button ref={this.next_ref} className="boton backgroundColor" onClick={this.next}>SIGUIENTE</button>
                </div>
                <div className="d-flex flex-row justify-content-center mt-3">
                    <Link to="/inicio" style={{ fontSize: '15px' }} className="text-muted">Saltar</Link>
                </div>
            </div>
        );
    }
}

export default Presentation;