import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Topbar extends Component {

    state = {
        '/inicio/perfil': 'TU PERFIL',
        '/inicio/recetas': 'COMIDAS CACERAS',
        '/inicio/categorias': 'CATEGORÍAS',
        '/inicio': ''
    }

    render() {
        let titulo = this.state[this.props.location];
        return (
            <div className="topbar backgroundColor d-flex bd-highlight" style={{ position: 'fixed', zIndex: '3', height: '56px' }}>
                <div className="bd-highlight ml-2">
                    <Link to='/inicio'>
                        <img className="h-100 topbar-icon ml-1 " src="/media/back.svg" style={{ visibility: this.props.location == '/inicio' ? 'hidden' : 'visible' }}/>
                    </Link>
                    <p className="h-100 topbar-icon" hidden={!this.props.location == '/inicio'}/>
                </div>
                <div className="flex-grow-1 bd-highlight">
                    <div className="d-flex justify-content-center align-items-center h-100 w-100 my-auto" style={{  maxHeight: '5em', height: '2.5em' }}>
                        {
                            titulo == '' ? <img className="logo-icon logo-icon-margin " src="/media/logo-blanco.svg"/> :
                                        <p className="titulo-topbar" style={{ fontSize: '14px', color: 'white' }} src="/media/logo-blanco.svg" hidden={titulo == ''}>{titulo}</p>
                        }
                    </div>
                </div>
                <div className="d-flex sidebar-icon bd-highlight mr-2">
                    <img className="h-100 topbar-icon" src="/media/menu.svg"/>
                </div>
            </div>
        )
    }
}

export default Topbar;