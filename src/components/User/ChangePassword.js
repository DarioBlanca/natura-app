import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

export class ChangePassword extends Component {

    state = {
        password1: '',
        password2: '',
        redirect: false
    }

    changePassword1 = (e) => this.setState({ password1: e.target.value });

    changePassword2 = (e) => this.setState({ password2: e.target.value });

    redirect = () => this.setState({ redirect: true })

    render() {
        if (this.state.redirect) return(<Redirect to='/exito'/>); else
        return(
            <div className="d-flex align-items-center justify-content-center h-100">
                <div className="d-flex flex-column w-100 h-100">
                    <div className="d-flex flex-row justify-content-center" style={{ marginTop: '80px' }}>
                        <p style={{ textAlign: 'center', width: '100%', fontSize: '16px' }}>CAMBIAR CONTRASEÑA</p>
                    </div>
                    <div className="d-flex flex-column justify-content-center" style={{ marginTop: '48px', marginRight: '32px', marginLeft: '32px' }}>
                        <div className="form-group d-flex flex-column">
                            <label className="mt-1" style={{fontWeight: 'bold', fontSize: '12px'}}>Nueva contraseña</label>
                            <input type="password" className="input-custom" onChange={this.changePassword1}/>
                        </div>
                        <div className="form-group d-flex flex-column h-100">
                            <label className="mt-1" style={{fontWeight: 'bold', fontSize: '12px'}}>Repetír contraseña</label>
                            <input type="password" className="input-custom" onChange={this.changePassword2}/>
                        </div>
                    </div>
                </div>
                <div className="d-flex flex-row justify-content-center align-items-bottom" style={
                        { position: 'absolute',
                          bottom: '0',
                          marginBottom: '48px' }}>
                    <button to={this.state.password1 && this.state.password2 && this.state.password1 == this.state.password2 ? '/exito' : false} 
                                className="boton backgroundColor" style={{ width: '280px', maxWidth: '65vw' }}
                         onClick={this.redirect}><p style={{ fontSize: '16px' }}>CONFIRMAR</p></button>
                </div>
            </div>
        )
    }
}