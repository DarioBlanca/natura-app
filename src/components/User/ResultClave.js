import React, { Component } from 'react'
import { Redirect } from 'react-router-dom';

export class ResultClave extends Component {

    state = {
        redirect: false
    }

    redirect = () => this.setState({ redirect: true })

    render() {
        if (this.state.redirect) return(<Redirect to='/inicio/perfil'/>); else
        return(
            <div className="d-flex align-items-center justify-content-center">
                <div className="d-flex flex-column w-100">
                    <div className="d-flex flex-row justify-content-center" style={{ marginTop: '100px' }}>
                        <img src='/media/success.svg'/>
                    </div>
                    <div className="d-flex flex-row justify-content-center" style={{ marginTop: '48px' }}>
                        <h3 className="single-slide-title text-justify text-center" style={{fontSize: '20px', width: '70%' }}>¡TU CONTRASEÑA HA SIDO MODIFICADA CON ÉXITO!</h3>
                    </div>
                    <div className="d-flex flex-row justify-content-center" style={{ marginTop: '10px' }}>
                        <p className="text-center text-justify" style={{ maxWidth: '75%', fontSize: '15px', color: '#666666'  }}>Ahora podés comenzar a disfrutar de nuestra App Natura para conocer las mejores recetas y tips.</p> 
                    </div>
                    <div className="d-flex flex-row justify-content-center" style={{ marginTop: '30%' }}>
                        <button to="/inicio/perfil" className="boton backgroundColor" onClick={this.redirect}>VOLVER</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default ResultClave;