import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom';

export class Login extends Component {

    state = {
        email: '',
        password: '',
        redirect: false
    }

    changeEmail = (e) => this.setState({ email: e.target.value })

    changePassword = (e) => this.setState({ password: e.target.value })

    validate = (e, input) => {}

    redirect = () => (this.state.email && this.state.password) && this.setState({ redirect: true });
    
    render() {
        if (this.state.redirect) return(<Redirect to='/inicio'/>); else
        return(
            <div className="login-body d-flex align-items-center">
                <div className="d-flex justify-content-center w-100 h-100">
                    <div className="form-container d-flex flex-column h-100" style={{ width: '80vw' }}>
                        <div className="w-100 d-flex justify-content-center" style={{ marginTop: '110px' }}>
                            <img src="./media/logo-verde.svg" style={{ width: '30vw', maxWidth: '10em' }}/>
                        </div>
                        <div className="" style={{marginTop: '10vh'}}>
                            <form className="form-container mb-5">
                                {/* <input className="input-custom text-gray" type="text" name="nombre" placeholder="Ingresá un email"/>
                                <input className="input-custom text-gray" type="password" name="clave" placeholder="Ingresá una clave"/>
                                <input className="input-custom text-gray" type="password" name="clave" placeholder="Repetír clave"/> */}

                                <div className="form-group d-flex flex-column">
                                    <input type="email" className="input-custom" onBlur={(e) => this.validate(e, 'email')} placeholder="Ingresá un correo" id="email" onChange={this.changeEmail}/>
                                </div>
                                <div className="form-group d-flex flex-column">
                                    <input type="password" className="input-custom" onBlur={(e) => this.validate(e, 'password')} placeholder="Ingresá una contraseña" id="password" onChange={this.changePassword}/>
                                </div>
                                <div className="d-flex flex-row w-100 justify-content-end">
                                    <Link to="/inicio" className="text-link" style={{fontSize:'0.9em'}}>Olvidaste tu contraseña?</Link>
                                </div>
                            </form>
                        </div>
                        <div className="d-flex h-100 align-items-end justify-content-center mb-5">
                            <div className="d-flex flex-column w-100">
                                <div className="row-2 w-100">
                                    <div className="d-flex flex-row w-100 h-100 align-items-end">
                                        <div className="d-flex w-100 justify-content-center">
                                            <button onClick={this.redirect} className="boton backgroundColor" style={{widht: '80% !important'}}>
                                                <p className="boton-link">INGRESAR</p>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div className="row-2 mt-3">
                                    <div className="d-flex flex-row w-100 h-100 align-items-end">
                                        <div className="d-flex w-100 justify-content-center">
                                            <Link to="/registro" className="text-link-green" onClick={this.register} style={{ borderBottom: 'none', background: 'white' }}>
                                                REGISTRARTE</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;