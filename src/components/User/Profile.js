import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import data from './data.json';
import CategoriasHorizontalList from '../Home/Categorias/CategoriasHorizontalList.js';

export class Profile extends Component {

    state = {
        data: (this.props.data ? this.props.data.user : data.user)
    }

    render() {
        const { data } = this.state;
        return(
            <div className="home-elements d-flex align-items-center mt-4">
                <div className="d-flex flex-column justify-content-center w-100">
                    <div className="d-flex w-100 justify-content-center">
                        <div className="form-container d-flex flex-column" style={{ width: '80vw' }}>
                            <div className="d-flex flex-column">
                                <div className="w-100 d-flex justify-content-center">
                                    <img src={data.picture} style={{ width: '35vw', maxWidth: '10em', borderRadius: '50%' }}/>
                                </div>
                                <div className="w-100 d-flex justify-content-center">
                                    <p className="mt-2 title" style={{fontWeight: 'bold'}}>{data.name + ' ' + data.last_name}</p>
                                </div>
                                <div className="" style={{marginTop: '5vh'}}>
                                    <form className="form-container mb-5">
                                        <div className="form-group d-flex flex-column">
                                            <label className="mt-1" style={{fontWeight: 'bold'}}>Nombre</label>
                                            <input type="text" className="input-custom" placeholder={data.name} id="name"/>
                                        </div>
                                        <div className="form-group d-flex flex-column">
                                            <label className="mt-1" style={{fontWeight: 'bold'}}>Apellido</label>
                                            <input type="text" className="input-custom" placeholder={data.last_name} id="last_name"/>
                                        </div>
                                        <div className="form-group d-flex flex-column">
                                            <label className="mt-1" style={{fontWeight: 'bold'}}>Correo</label>
                                            <input type="email" className="input-custom" placeholder={data.email} id="email"/>
                                        </div>
                                        <div className="form-group d-flex flex-column">
                                            <label className="" style={{fontWeight: 'bold'}}>Clave</label>
                                            <div className="d-flex flex-row-reverse mr-1">
                                                <Link className="text-link" to="/clave" style={{ position: 'absolute', zIndex: '2', borderBottom: '0px' }}>Editar</Link>
                                                <input type="password" className="input-custom" placeholder='***********' id="email"/>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <CategoriasHorizontalList show_more={false} title='INTERESES'/>
                </div>
            </div>
        );
    }
}

export default Profile;