import React, { Component } from 'react'
import { Link } from 'react-router-dom';

export class PostRegister extends Component {

    state = {
        image: this.props.ok ? '/media/success.svg' : '/media/error.svg',
        titulo: this.props.ok ? '¡TU CUENTA FUÉ CREADA CON ÉXITO!' : 'LO SENTIMOS, OCURRIÓ UN ERROR',
        descripcion: this.props.ok ? 'Ahora podés comenzar a disfrutar de nuestra App Natura para conocer las mejores recetas y tips.' 
                                   : 'No se puedo crear tu cuenta de manera éxitosa, por favor intenta más tarde.',
        boton: this.props.ok ? 'INGRESAR' : 'VOLVER A INTENTAR',
    }

    render() {
        return(
            <div className="d-flex align-items-center justify-content-center">
                <div className="d-flex flex-column w-100">
                    <div className="d-flex flex-row justify-content-center" style={{ marginTop: '100px' }}>
                        <img src={this.state.image}/>
                    </div>
                    <div className="d-flex flex-row justify-content-center" style={{ marginTop: '48px' }}>
                        <h3 className="single-slide-title" style={{fontSize: '20px' }}>{this.state.titulo}</h3>
                    </div>
                    <div className="d-flex flex-row justify-content-center" style={{ marginTop: '10px' }}>
                        <p className="text-center text-justify" style={{ maxWidth: '75%', fontSize: '15px', color: '#666666'  }}>{this.state.descripcion}</p> 
                    </div>
                    <div className="d-flex flex-row justify-content-center" style={{ marginTop: '114px' }}>
                        <Link to={this.props.ok ? '/inicio' : '/registro' } className="boton backgroundColor" onClick={this.next}>{this.state.boton}</Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default PostRegister;