import React, { Component } from 'react';
import { Search } from './Search/Search';
import SliderCarousel from './Slider/SliderCarousel';
import CategoriasHorizontalList from './Categorias/CategoriasHorizontalList';
import RecetasCarousel from './Recetas/RecetasCarousel';


export class Home extends Component {

    render() {
        return(
            <div className="home-elements bg-white mt-1" style={{ marginBottom: '3.5rem' }}>
                <Search/>
                <SliderCarousel/>
                <CategoriasHorizontalList title="CATEGORÍAS MÁS VISITADAS" show_more={true}/>
                <p className="title text-dark mt-3">RECETAS DESTACADAS</p>
                <RecetasCarousel/>
            </div>
        )
    }
}