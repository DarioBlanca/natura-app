import React, { Component } from 'react'
import Collapse from 'react-collapse';
import { RecetasCarousel } from './RecetasCarousel';
import { Link } from 'react-router-dom';
export class Receta extends Component {

    state = {
        data: this.props.data ? this.props.data : JSON.parse(localStorage.getItem('receta')),
        ingredients: false,
        preparation: false,
    }

    carousel = React.createRef();

    componentWillUpdate() {
        this.carousel.current.reset();
    }

    componentdDidMount() {
        window.scrollTo(0, 0);
    }

    componentDidUpdate() {
        window.scrollTo(0, 0);
    }

    points = (cant) => {
        cant = Math.floor(parseFloat(cant));
        let result = []
        for (let i = 0; i < cant; i++) {
            result.push(<img src="/media/star.svg" style={{ width: '12px', height: '12px' }}/>)
        }
        return result;
    }

    ingredients = (ingredients) =>
        <div class="receta-content-card">
            <div class="d-flex bd-highlight" style={{ background: '#756B40' }} onClick={() => this.setState({ ingredients: !this.state.ingredients })}>
                <div class="mr-auto bd-highlight" style={{ padding: '1em', marginLeft: '3%' }}>
                    <span style={{ fontSize: 'calc(0.2rem + 3vw)', color: 'white', fontFamily: 'MediumCond' }}>INGREDIENTES</span>
                </div>
                <div class="bd-highlight">
                    <div className="h-100 d-flex align-items-center mr-2">
                        <img className="arrow" src={!this.state.ingredients ? '/media/arrow-down.svg': '/media/arrow-up.svg'} />
                    </div>
                </div>
            </div>

            <Collapse isOpened={this.state.ingredients}>
                <div id="collapseOne" class="collapse show" aria-labelledby="ingredients" data-parent="#accordionExample">
                    <div class="card-body" style={{ textAlign:'left' }}>
                        <ul style={{ listStyle:'none', paddingLeft: '20px' }}>
                            {ingredients.map(ingredient => <li className="mt-3">{ingredient}</li>)}
                        </ul>
                    </div>
                </div>
            </Collapse>
        </div>;

    preparation = (preparation) => {
        let paragraphs = preparation.split('\n\n');
        return(
            <div class="receta-content-card" style={{ backgroundColor: '#F5F3EE' }}>
                <div class="d-flex bd-highlight" style={{ background: '#E3D752' }} onClick={() => this.setState({ preparation: !this.state.preparation })}>
                <div class="mr-auto bd-highlight" style={{ padding: '1em', marginLeft: '3%' }}>
                    <span style={{ fontSize: 'calc(0.2rem + 3vw)', color: 'white', fontFamily: 'MediumCond' }}>PREPARACIÓN</span>
                </div>
                <div class="bd-highlight">
                    <div className="h-100 d-flex align-items-center mr-2">
                        <img className="arrow" src={!this.state.preparation ? '/media/arrow-down.svg': '/media/arrow-up.svg'} />

                    </div>
                </div>
            </div>

                <Collapse isOpened={this.state.preparation}>
                    <div id="collapseOne" class="collapse show" aria-labelledby="preparation" data-parent="#accordionExample">
                        <div class="card-body" style={{ textAlign:'left' }}>
                            {paragraphs.map(paragraph => <p className="mt-3">{paragraph}</p>)}                    
                        </div>
                    </div>
                </Collapse>

            </div>);
    }


    render() {
        const { data } = this.state;
        if (!data) return(<div>No data</div>); else
        return(
            <div className="receta w-100">
                <div className="d-flex flex-row w-100 ml-2" style={{ height: '56px', position: 'absolute' }}>
                    <Link to="/inicio/recetas" style={{ zIndex: '5' }}>
                        <img className="h-100 topbar-icon ml-1" src="/media/back.svg"/>
                    </Link>
                </div>
                <div className="receta-img" style={{ opacity: '0.28',	backgroundColor: '#333333', position: 'absolute' }}/>
                <img className="receta-img" src={data.image}/>
                <div className="receta-header d-flex align-items-center bg-white w-100 mt-2">
                    <div className="d-flex justify-content-center w-100">
                        <div className="d-flex flex-column w-100">
                            <span style={{ color: '#9E9E9E', fontSize: '10px', fontSize: '10px', fontWeight: 'bold', letterSpacing: '1px' }}>{data.category}</span>
                            <h3 className="text-justify text-center mt-2" style={{ color: '#AD9C7E' }}>{data.title.toUpperCase()}</h3>
                            <div className="d-flex flex-row justify-content-center w-100">
                                <div className="d-flex flex-row justify-content-center align-items-center w-100">
                                    {this.points(data.points)}
                                    <img src="/media/clock.svg" style={{ width: '12px', height: '12px', marginLeft: '4%', marginRight: '2%' }}/>
                                    <span className="receta-inline align-middle">{data.minutes + ' minutos'}</span>
                                    <img src="/media/spoon.svg" style={{ width: '12px', height: '12px', marginLeft: '4%', marginRight: '2%' }}/>
                                    <span className="receta-inline align-middle">{data.portions + ' porciones'}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="w-100 accordion mt-3" id="accordionExample">
                    {this.ingredients(data.ingredients)}
                    {this.preparation(data.preparation)}
                </div>
                <p className="title text-dark mt-3">CATEGORÍAS MÁS DESTACADAS</p>
                <RecetasCarousel ref={this.carousel}/>
            </div>
        );
    }
} 

export default Receta;