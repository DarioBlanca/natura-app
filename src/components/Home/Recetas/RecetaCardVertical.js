import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class RecetaCardVertical extends Component {

    render() {
        const { data } = this.props;
        return(
            <div className="card-root">
                <div className="card border-white text-center" style={{ maxWidth: '100%', marginLeft: '0%' }}>
                    <img className="card-img-top mh-50" src={data.image}/>
                    <div className="card-body w-100 receta-card-body">
                        <div className="receta-header d-flex align-items-center bg-white w-100 mt-2">
                            <div className="d-flex align-item-center w-100">
                                <div className="d-flex flex-column mw-100 w-100">
                                    <div className="d-flex flex-row w-100">
                                        <div className="mr-auto">
                                            <div className="d-flex justifty-content-start">
                                                <p className="receta-card-category">{data.category}</p>
                                            </div>
                                        </div>
                                        <div className="">
                                            <div className="d-flex justifty-content-end">
                                                <img src="/media/star.svg" style={{ width: '14px', height: '14px' }}/>
                                                <p className="receta-card-category ml-1">{data.points}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <h3 className="card-text text-justify text-truncate receta-card-title">{data.title}</h3>
                                    <div className="d-flex flex-row justify-content-start">
                                        <div className="d-flex flex-column align-item-center w-100">
                                            <div className="d-flex flex-row">
                                                <img src="/media/clock.svg" style={{ width: '12px', height: '12px' }}/>
                                                <span className="receta-card-text ml-1">{data.minutes + ' minutos'}</span>
                                                <img src="/media/spoon.svg" style={{ width: '12px', height: '12px', marginLeft: '4%' }}/>
                                                <span className="receta-card-text ml-1">{data.portions + ' porciones'}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="d-flex flex-row">
                                        <Link to="/receta" className="mt-3 text-link">Ver más</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
} 

export default RecetaCardVertical;