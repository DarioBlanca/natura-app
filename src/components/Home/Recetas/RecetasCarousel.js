import React, { Component } from 'react';
import data from './data.json';
import ScrollMenu from 'react-horizontal-scrolling-menu';
import { Redirect } from 'react-router-dom';
import RecetaCardVertical from './RecetaCardVertical.js';

export class RecetasCarousel extends Component {

    state = {
        items: data.recetas.map((data, index) => <RecetaCardVertical data={data} key={index}/>),
        redirect: false,
    }

    onSelect = () => this.setState({ redirect: true })

    reset = () => this.setState({ redirect: false });

    render() {
        localStorage.setItem('receta', JSON.stringify(data.recetas[7]));
        const config = {
            data: this.state.items,
            dragging: true,
            alignCenter: false,
            menuClass: { width: '100%', height: '50%' },
            onSelect: this.onSelect
        }
        if (this.state.redirect) return(<Redirect to="/receta"/>); else
        return (
            <div className="recetasCarousel-root bg-white w-100">
                <div className="mt-3 w-100">
                    <ScrollMenu {...config}/>
                </div>
            </div>
        );
    }
}

export default RecetasCarousel;