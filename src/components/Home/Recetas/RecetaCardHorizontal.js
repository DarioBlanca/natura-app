import React, { Component } from 'react';

export class RecetaCardHorizontal extends Component {

    points = (cant) => {
        cant = Math.floor(parseFloat(cant));
        let result = []
        for (let i = 0; i < cant; i++) {
            result.push(<img src="/media/star.svg" style={{ width: '12px', height: '12px' }}/>)
        }
        return result;
    }

    toTitleCase(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1);
    }

    render() {
        const { data } = this.props
        return(
            <div class="card receta-horizontal-card border-white bg-white" onClick={this.props.redirect} style={{ maxHeight: '20vh' }}>
                <div class="d-flex flex-row no-gutters mh-100" style={{ height: '125px' }}>
                    <div class="col-5" style={{ maxHeight: '100%' }}>
                        <img src={data.image} class="card-img receta-card-img receta-lista" alt={data.title} style={{ borderRadius: '4px 1px 1px 5px' }}/>
                    </div>
                    <div class="col-7" style={{ maxHeight: '100%' }}>
                        <div class="card-body" style={{ padding: '12px' }}>
                            <div className="receta-header d-flex align-items-center bg-white w-100">
                                <div className="d-flex align-item-center w-100">
                                    <div className="d-flex flex-column">
                                        <h3 className="receta-card-title">{this.toTitleCase(data.title.toLowerCase())}</h3>
                                        <div className="d-flex flex-row justify-content-start">
                                            <div className="d-flex flex-column align-item-center w-100">
                                                <div className="d-flex flex-row mt-1">
                                                    {this.points(data.points)}
                                                </div>
                                                <div className="d-flex flex-row mt-2">
                                                    <img src="/media/clock.svg" style={{ width: '12px', height: '12px' }}/>
                                                    <span className="receta-card-text ml-1">{data.minutes + ' minutos'}</span>
                                                </div>
                                                <div className="d-flex flex-row mt-1">
                                                    <img src="/media/spoon.svg" style={{ width: '12px', height: '12px' }}/>
                                                    <span className="receta-card-text ml-1">{data.portions + ' porciones'}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default RecetaCardHorizontal;