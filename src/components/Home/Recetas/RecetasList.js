import React, { Component } from 'react'
import data from './data.json';
import { RecetaCardHorizontal } from './RecetaCardHorizontal.js';
import { Redirect } from 'react-router-dom';
import { Search } from '../Search/Search';
import { Link } from 'react-router-dom';

export class RecetasList extends Component {

    state = {
        items: [],
        redirect: false
    }

    redirect = () => this.setState({ ...this.state, redirect: true });

    componentDidMount() {
        window.scrollTo(0, 0);
        this.setState({ items: data.recetas.map((element, index) => <RecetaCardHorizontal data={element} key={index} redirect={this.redirect}/>)})
    }

    render() {
        if (this.state.redirect) return(<Redirect to='/receta'/>); else
        return(
            <div className="mt-1">
                <Search/>
                <div className="recetas-list-container mt-3">
                    <div className="d-flex flex-row align-items-center" style={{ marginLeft: '4%', marginRight:'4%' }}>
                        <Link to='recetas' className="text-link">Recetas </Link><p className="ml-1">></p>
                        <Link to='recetas' className="text-link ml-1">Comidas caceras</Link><p className="ml-1">></p>
                        <p to='recetas' className="texto ml-1">Pastas</p>
                        {/* <div className="d-flex w-100 justify-content-end">
                            <img ></img>
                        </div> */}
                    </div>    
                    <div className="recetas-list d-flex flex-column justify-content-center last">
                        {this.state.items.map(element => element)}
                    </div>                
                </div>
            </div>
        )
    }
}