import React, { Component } from 'react'
import data from './data.json';
import { Categoria } from './Categoria';
import { Redirect } from 'react-router-dom';
import { Search } from '../Search/Search';
import StackGrid, { transitions } from "react-stack-grid";
import sizeMe from 'react-sizeme';

const { scaleDown } = transitions;

export class CategoriasList extends Component {

    state = {
        items: data.categories.map((element, index) => <Categoria data={element} key={index} redirect={this.redirect} forGrid={true} no_width={true}/>),
        final: '',
        redirect: false,
        grid: ''
    }

    redirect = () => this.setState({ ...this.state, redirect: true });

    componentDidMount() {
    }

    updateGrid = (grid) => {
        console.log('UPDATE');
        setTimeout(() => grid.updateLayout(), 300);
    }

    render() {
        let i = 0;
        if (this.state.redirect) return(<Redirect to='/inicio/receta'/>); else
        return(
            <div className="mt2">
                <Search/>
                <div className="recetas-list-container mt-2" style={{ marginLeft: '3%', marginRight: '3%' }}>
                    <StackGrid gridRef={grid => this.updateGrid(grid)}
                               columnWidth={'33.33%'}>
                        {this.state.items.map(element => element)}
                    </StackGrid>        
                </div>
            </div>
        )
    }
}

export default sizeMe()(CategoriasList);