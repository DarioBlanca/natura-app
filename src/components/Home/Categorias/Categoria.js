import React from 'react'

export const Categoria = (props) => 
    <div className="categoria-container">
        <div className="categoria-root d-flex align-items-center justify-content-center" style={{ width: props.no_width ? '100%' : '25vw' }}>
            <div className="d-flex flex-column">
                <div className="d-flex flex-row justify-content-center">
                    <img className="categoria-img my-auto" src={props.data.icon}/>
                </div>
                <p className="categoria-name">{props.data.name}</p>
            </div>
        </div>
    </div>;

export default Categoria;