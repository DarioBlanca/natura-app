import React, { Component } from 'react';
import ScrollMenu from 'react-horizontal-scrolling-menu';
import { Categoria } from './Categoria';
import { Link } from 'react-router-dom';
import data from './data.json';

export class CategoriasHorizontalList extends Component {

    state = {
        items: data.categories.map((data, index) => <Categoria data={data} key={index} no_width={false}/>),
        show_more: this.props.show_more || false,
        title: this.props.title || 'CATEGORIAS'
    }

    render() {
        const config = {
            data: this.state.items,
            dragging: true,
            alignCenter: false,
            menuClass: { width: '100%', height: '50%' }
        }
        return(
            <div className="categorias-list-container last w-100" style={{ backgroundColor: '#F5F3EE', paddingTop: '6%', paddingBottom: '5%' }}>
                <p className="title text-dark">{this.state.title}</p>
                <div className="w-100">
                    <ScrollMenu {...config}/>
                </div>
                <p style={{ marginLeft: '4vw' }}><Link className="text-link" to={'/inicio/categorias'} hidden={!this.state.show_more}>Ver todas las categorias</Link></p> 
            </div>
        )
    }
}

export default CategoriasHorizontalList;