import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import Select from 'react-dropdown-select';
import { options } from "./options";

export class Search extends Component {

    state = {
        redirect: false,
    }

    onSelect(value) {
        // this.setState({ redirect: true });
    }

    render() {
        if (this.state.redirect) return(<Redirect/>)
        return(
            <div className="w-100 bg-white">
                <form onSubmit={this.onFormSubmit}>
                    <div className="d-flex justify-content-end search-container">
                        <Select
                            placeholder="Buscar recetas"
                            className="search-input shadow"
                            style={{ fontSize: '15px' }}
                            options={options}
                            onChange={(value) => this.onSelect(value)}
                            />
                        {/* <input className="search-input shadow" type="text" placeholder="Buscar receta" style={{ position: 'relative' }}/> */}
                        <button type="submit" hidden/>
                    </div>
                </form>
                {/* <div style={{ position: 'absolute', alignSelf: 'center', marginTop: '4%'}}>
                    <div className="d-flex justify-content-end">
                        <img src="./media/search.svg" style={{ width: '8%', marginTop: '5%' }}/>
                    </div>
                </div> */}
            </div>
        )
    }
}