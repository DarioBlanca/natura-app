import React, { Component } from 'react';
import { Topbar } from '../../Topbar/Topbar';
import { Toolbar } from '../../Toolbar/Toolbar';
import { Route } from 'react-router-dom';
import { RecetasList } from '../Recetas/RecetasList';
import { Home } from '../Home';
import Profile from '../../User/Profile';
import CategoriasList from '../Categorias/CategoriasList';
import {withRouter} from 'react-router-dom';

export class Inicio extends Component {
    
    render() {
        return(
            <div>
                <Topbar location={this.props.location.pathname}/>
                <div className="topbar-simulated-space"/>
                <Route path='/inicio/perfil' component={Profile}/>
                <Route path='/inicio/recetas' component={RecetasList}/>
                <Route path='/inicio/categorias' component={CategoriasList}/>
                <Route exact path='/inicio' component={Home}/>
                <Toolbar location={this.props.location.pathname}/>
            </div>
        )
    }
}

export default withRouter(Inicio);