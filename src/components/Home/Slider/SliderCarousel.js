import React, { Component } from 'react';
import Carousel from 'react-bootstrap/Carousel'
import data from './data.json';
import { Link } from 'react-router-dom';

export class SliderCarousel extends Component {

    state = {
        items: data.slides.map((data, index) => 
            <Carousel.Item className="w-100 h-25" key={index}>
                <img className="mw-100 mh-100" src={data.image}/>
                <Carousel.Caption className="slide-content h-100 d-flex align-items-center ml-3" style={{ zIndex:1 }}>
                    <div className="d-flex flex-column align-start">                
                        <span className="slideCarousel-title text-dark" style={{ lineHeight: '0.9' }}>{data.title}</span>
                        <span className="slideCarousel-description text-secondary">{data.description}</span>
                        <div className="d-flex flex-row">
                            <Link to={data.link_url} className="slideCarousel-url text-link">{data.link_title}</Link> 
                        </div> 
                    </div>
                </Carousel.Caption>
            </Carousel.Item>)
    }

    componentDidMount() {

    }

    render() {
        const settings = {
            indicators: false,
            nextIcon: null,
            prevIcon: null,
            interval: 3000,
            draggable: true,
        };
        return(
            <div className="w-100 h-25 slider-root bg-white">
                <Carousel {...settings}>
                    {this.state.items.map(element => element)}
                </Carousel>
            </div>
        )
    }
}

export default SliderCarousel;