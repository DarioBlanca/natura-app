import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { fadeIn } from 'react-animations';
import Radium, {StyleRoot} from 'radium';

const styles = {
    fadeIn: {
        animation: 'x 2s',
        animationName: Radium.keyframes(fadeIn, 'fadeIn')
    }
}

export class Splash extends Component {

    state = {
        redirect: false,
    }

    componentDidMount() {
        setTimeout(() => this.setState({ redirect: true }), 3000);
    }

    render() {
        if (this.state.redirect) return(<Redirect to="/presentacion"/>); else 
        return(
            <div>
                <StyleRoot>
                    <img src="./media/splash.png" className="splash" style={styles.fadeIn}/>
                </StyleRoot>
            </div>
        );
    }
}

export default Splash;