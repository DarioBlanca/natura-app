import React from 'react';
import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Presentation } from './components/Presentation/Presentation';
import Inicio from './components/Home/Inicio/Inicio';
import { Receta } from './components/Home/Recetas/Receta';
import { Splash } from './components/Splash/Splash';
import { Register } from './components/User/Register';
import { Login } from './components/User/Login'; 
import { LoadResult } from './components/User/LoadResult';
import { ChangePassword } from './components/User/ChangePassword';
import { ResultClave } from './components/User/ResultClave';

function App() {

  return (
    <BrowserRouter>       
      <div>
          <Switch>
            <Route path='/presentacion' component={Presentation}/>
            <Route path='/inicio' component={Inicio}/>
            <Route path='/receta' component={Receta}/>
            <Route path='/registro' component={Register}/>
            <Route path='/resultado' component={LoadResult}/>
            <Route path='/login' component={Login}/>
            <Route path='/clave' component={ChangePassword}/>
            <Route path='/exito' component={ResultClave}/>
            <Route path='/' component={Splash}/>
          </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
